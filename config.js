/*eslint no-undef: "off"*/

let config = {};

config.env = process.env.NODE_ENV || 'development';

module.exports = config;
