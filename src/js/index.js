import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/style.styl';

import App from './App.js';

ReactDOM.render(<App />, document.getElementById('react_container'));
