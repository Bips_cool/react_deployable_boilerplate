import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';
import { Button, notification, Checkbox, Menu, Dropdown, Icon, message, Modal, Form, Input, Radio, Divider, Drawer } from 'antd';

const CheckboxGroup = Checkbox.Group;
const Search = Input.Search;
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
	class extends React.Component {
		render() {
			const { visible, onCancel, onCreate, form } = this.props;
			const { getFieldDecorator } = form;
			return (
				<Modal
					visible={visible}
					title="Create a new collection"
					okText="Create"
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout="vertical">
						<FormItem label="Title">
							{getFieldDecorator('title', {
								rules: [{ required: true, message: 'Please input the title of collection!' }],
							})(
								<Input />
							)}
						</FormItem>
						<FormItem label="Description">
							{getFieldDecorator('description')(<Input type="textarea" />)}
						</FormItem>
						<FormItem className="collection-create-form_last-form-item">
							{getFieldDecorator('modifier', {
								initialValue: 'public',
							})(
								<Radio.Group>
									<Radio value="public">Public</Radio>
									<Radio value="private">Private</Radio>
								</Radio.Group>
							)}
						</FormItem>
					</Form>
				</Modal>
			);
		}
	}
);

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			loading: false,
			iconLoading: false,
			drawerVisible: false,
			showHighcharts: ['highcharts']
		};

		this.plainOptions = ['Apple', 'Pear', 'Orange'];
		this.options = [
			{ label: 'Apple', value: 'Apple' },
			{ label: 'Pear', value: 'Pear' },
			{ label: 'Orange', value: 'Orange' },
		];
		this.optionsWithDisabled = [
			{ label: 'Apple', value: 'Apple' },
			{ label: 'Pear', value: 'Pear' },
			{ label: 'Orange', value: 'Orange', disabled: false },
		];

		this.menu = <Menu onClick={(e) => this.handleMenuClick(e)}>
			<Menu.Item key="1"><Icon type="user" />1st menu item</Menu.Item>
			<Menu.Item key="2"><Icon type="user" />2nd menu item</Menu.Item>
			<Menu.Item key="3"><Icon type="user" />3rd item</Menu.Item>
		</Menu>;

		this.trending_graph = {
			chart: {
				zoomType: 'x',
				height: (this.state.view_mode === 'desktop') ? (window.innerHeight - 285) * 0.6 : 300,
				backgroundColor: '#fff'
			},
			// colors: ['#f4801f'],
			title: {
				text: 'Parameter',
				style: {
					fontSize: '14px'
				}
			},
			xAxis: {
				type: 'datetime',
				text: 'Time'
			},
			yAxis: {
				title: {
					text: 'Unit'
				},
				floor: 0,
				plotLines: []
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Label : <b>{point.y}</b>'
			},
			plotOptions: {
				area: {
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},
			series: [{
				type: 'area',
				data: [
					[1533724600, 123],
					[1533724650, 234],
					[1533724700, 345],
					[1533724750, 78],
					[1533724800, 89]
				],
				color: '#F4801F',
				fillColor: '#FFDCC199'
			}]
		};
	}

	handleButtonClick(e) {
		message.info('Click on left button.');
		console.log('click left button', e);
	}

	handleMenuClick(e) {
		message.info('Click on menu item.');
		console.log('click', e);
	}

	onChange(checkedValues) {
		console.log('checked = ', checkedValues);
		this.setState({showHighcharts: checkedValues});
	}

	close() {
		console.log('Notification was closed. Either the close button was clicked or duration time elapsed.');
	}

	showModal() {
		this.setState({ visible: true });
	}

	handleCancel() {
		this.setState({ visible: false });
	}

	handleCreate() {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}

			console.log('Received values of form: ', values);
			form.resetFields();
			this.setState({ visible: false });
		});
	}

	saveFormRef(formRef) {
		this.formRef = formRef;
	}

	openNotification() {
		const key = `open${Date.now()}`,
			btn = <Button type="primary" size="small" onClick={() => notification.close(key)}>Confirm</Button>;
		notification.open({
			message: 'Notification Title',
			description: 'A function will be be called after the notification is closed (automatically after the "duration" time of manually).',
			btn,
			key,
			onClose: this.close,
		});
	}

	enterLoading() {
		this.setState({ loading: true });
	}

	enterIconLoading() {
		this.setState({ iconLoading: true });
	}

	componentDidMount() {
		document.title = 'Dashboard';
	}

	render() {
		ReactHighcharts.Highcharts.setOptions({
			global: {
				useUTC: false
			}
		});

		return <div className="" id="dashboard_container">
			<Scrollbars autoHide>
				<div className="div">
					<Search onChange={(e) => console.log('Search:', e.target.value)} />
				</div>
				<div className="div">
					<Button type="primary" loading={this.state.loading} onClick={() => this.enterLoading()}>Show Loading</Button>
				</div>
				<Divider dashed />
				<div className="div">
					<Button type="primary" onClick={() => this.openNotification()}>Open the notification box</Button>
				</div>
				<Divider dashed />
				<div className="div">
					<Dropdown overlay={this.menu}>
						<Button style={{ marginLeft: 8 }}>
							<Icon type="user" /> Button <Icon type="down" />
						</Button>
					</Dropdown>
				</div>
				<Divider>Checkbox</Divider>
				<div className="div">
					<CheckboxGroup options={this.options} defaultValue={['Pear']} onChange={(e) => this.onChange(e)} />
					<Divider type="vertical" />
					<CheckboxGroup options={this.optionsWithDisabled} disabled defaultValue={['Apple']} onChange={(e) => this.onChange(e)} />
				</div>
				<Divider>
					<CheckboxGroup options={[{label: 'Show Highcharts', value: 'highcharts'}]} defaultValue={this.state.showHighcharts} onChange={(e) => this.onChange(e)} />
				</Divider>
				<div className="div">
					{(() => {
						if (this.state.showHighcharts.length) {
							return <ReactHighcharts config={this.trending_graph} />;
						}
					})()}
				</div>
				<Divider orientation="left">Modal</Divider>
				<div className="div">
					<Button type="primary" onClick={() => this.showModal()}>Show Popup</Button>
					<CollectionCreateForm
						wrappedComponentRef={(e) => this.saveFormRef(e)}
						visible={this.state.visible}
						onCancel={() => this.handleCancel()}
						onCreate={() => this.handleCreate()}
					/>
				</div>
				<Divider orientation="left">Drawer</Divider>
				<div className="div">
					{(() => {
						if (this.state.drawerVisible) {
							return <Drawer
								title="Basic Drawer"
								placement="right"
								closable={true}
								visible={true}
								onClose={() => this.setState({drawerVisible: false})}>
								<span>Hello Drawer</span>
							</Drawer>;
						} else {
							return <Button onClick={() => this.setState({drawerVisible: true})}>Show Drawer</Button>;
						}
					})()}
				</div>
			</Scrollbars>
		</div>;
	}
}
