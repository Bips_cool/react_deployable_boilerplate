import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import Dashboard from './components/Dashboard';

const Error = () => (<h3><center>404 - Not Found</center></h3>);

export default class App extends React.Component {
	render() {
		return <Router>
			<Switch>
				<Route exact path='/' component={Dashboard}/>
				<Route component={Error}/>
			</Switch>
		</Router>;
	}
}
