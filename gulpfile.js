'use strict';

//Dependencies
const fs = require('fs'),
	rimraf = require('rimraf'),
	gulp = require('gulp'),
	string_replace = require('gulp-string-replace'),
	htmlmin = require('gulp-htmlmin'),
	rename = require('gulp-rename'),
	sri = require('gulp-sri'),
	hash = require('gulp-hash'),
	gutil = require('gulp-util'),
	ftp = require('vinyl-ftp')/*,
	ftpKey = require('./ftp.key')*/;

// Build Type set through Node Environment variable
const build_type = process.env.BUILD_TYPE || 'local';
// const build_type = 'stage';
// const build_type = 'deploy';

//cdn base
const cdn_base = (build_type === 'deploy' ? '' : '');

//api_files
gulp.task('api_files', () => {
	gulp
		.src('./src/api/datoms/iocl/*.php')
		.pipe(
			string_replace(/\$api_utilities = new Utilities\(\[\'environment\' => \'development\'\]\);/, '$api_utilities = new Utilities();')
		)
		.pipe(
			gulp.dest('./dist/api/')
		);
});

//php file
gulp.task('php_file', () => {
	let src = 'src/index.html';
	let files_sri = JSON.parse(fs.readFileSync('./dist/cdn/sri.json', 'utf8'));
	let hashed_file_names = JSON.parse(fs.readFileSync('./dist/cdn/hash_manifest.json', 'utf8'));
	// let css_file_link = '<link href="'+ cdn_base + hashed_file_names['style.css'] +'" integrity="'+ files_sri['cdn/style.css'] +'" media="all" rel="stylesheet" crossorigin="anonymous" />';
	//let css_file_link = '<link href="'+ cdn_base + hashed_file_names['/style.css'] +'" media="all" rel="stylesheet" />';
	let js_preload_link = '<link as="script" href="'+ cdn_base + hashed_file_names['bundle.js'] +'" rel="preload" />';
	let js_file_link = '<script src="'+ cdn_base + hashed_file_names['bundle.js'] +'" integrity="'+ files_sri['dist/bundle.js'] +'" crossorigin="anonymous"></script>';
	// let js_file_link = '<script src="'+ cdn_base + hashed_file_names['bundle.js'] +'"></script>';
	//let backend_content_inside_head = css_file_link + js_preload_link;
	let backend_content_inside_head = js_preload_link;
	let backend_content_at_end_of_body = (build_type === 'deploy') ? fs.readFileSync('./src/php_content.php', 'utf8') : '';
	backend_content_at_end_of_body += js_file_link;

	gulp
		.src(src)
		.pipe(
			string_replace('<!-- ##PR_REPLACE_BY_BACKEND_CONTENT_INSIDE_HEAD## -->', backend_content_inside_head)
		)
		.pipe(
			string_replace('<!-- ##PR_REPLACE_BY_BACKEND_CONTENT_BEFORE_BODY_END## -->', backend_content_at_end_of_body)
		)
		.pipe(
			htmlmin({
				removeComments: true,
				collapseWhitespace: true,
				collapseBooleanAttributes: true,
				// removeTagWhitespace: true,
				removeAttributeQuotes: true,
				removeRedundantAttributes: true,
				removeEmptyAttributes: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true/*,
				minifyJS: true,
				processScripts: ['text/javascript'],
				minifyCSS: true,*/
				// ignoreCustomFragments: true/* use this to conserve php */
			})
		)
        .pipe(
        	rename('index.php')
        )
		.pipe(
			gulp.dest('./dist')
		);
});

//static assets
//generate hash
gulp.task('generate_hashes_for_css_and_js_files', () => {
	rimraf('./dist/cdn/', [], () => {
		return gulp
			.src([
				'./dist/bundle.js'
			])
			.pipe(
				hash({
					algorithm: 'sha512',
					hashLength: 64
				})
			)
			.pipe(
				gulp.dest('./dist/cdn/')
			)
			.pipe(
				hash.manifest('hash_manifest.json')
			)
			.pipe(
				gulp.dest('./dist/cdn/')
			);
	});
});
//generate sri
gulp.task('generate_sri_for_css_and_js_files', ['generate_hashes_for_css_and_js_files'], () => {
	return gulp
		.src([
			'./dist/bundle.js'
		])
		.pipe(
			sri({
				algorithms: ['sha384']
			})
		)
		.pipe(
			gulp.dest('./dist/cdn/')
		)
});

// Change the React Router
gulp.task('react_browser_router', () => {
	return gulp
		.src(['./src/js/App.js'])
		.pipe(string_replace('HashRouter', 'BrowserRouter'))
		.pipe(gulp.dest('./src/js/'))
});
gulp.task('react_hash_router', () => {
	return gulp
		.src(['./src/js/App.js'])
		.pipe(string_replace('BrowserRouter', 'HashRouter'))
		.pipe(gulp.dest('./src/js/'))
});

/* Upload files through FTP Connection */
/*// Primary Server FTP Connection
function getPrimaryServerFtpConnection() {
	return ftp.create({
		host: ftpKey.primary.host,
		port: ftpKey.primary.port,
		user: ftpKey.primary.user,
		password: ftpKey.primary.pass,
		parallel: 5,
		log: gutil.log
	});
}
// Secondary Server FTP Connection
function getSecondaryServerFtpConnection() {
	return ftp.create({
		host: ftpKey.secondary.host,
		port: ftpKey.secondary.port,
		user: ftpKey.secondary.user,
		password: ftpKey.secondary.pass,
		parallel: 5,
		log: gutil.log
	});
}
// Testing Server FTP Connection
function getTestingServerFtpConnection() {
	return ftp.create({
		host: ftpKey.staging.host,
		port: ftpKey.staging.port,
		user: ftpKey.staging.user,
		password: ftpKey.staging.pass,
		parallel: 5,
		log: gutil.log
	});
}*/

// Upload the JS files to primary server
gulp.task('upload_js_files_to_primary_server', () => {
	var conn = getPrimaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/cdn/*.js'],
		serverPath = '/phoenixrobotix/prstatic/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload the PHP files to primary server
gulp.task('upload_php_files_to_primary_server', () => {
	var conn = getPrimaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/index.php'],
		serverPath = '/phoenixrobotix/src/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload the API files to primary server
gulp.task('upload_api_files_to_primary_server', () => {
	var conn = getPrimaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/api/*.php'],
		serverPath = '/phoenixrobotix/api/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload files to primary server
gulp.task('upload_files_to_primary_server', [
	/*'upload_js_files_to_primary_server',
	'upload_php_files_to_primary_server',
	'upload_api_files_to_primary_server'*/
]);

// Upload the JS files to secondary server
gulp.task('upload_js_files_to_secondary_server', () => {
	var conn = getSecondaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/cdn/*.js'],
		serverPath = '/phoenixrobotix/prstatic/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload the PHP files to secondary server
gulp.task('upload_php_files_to_secondary_server', () => {
	var conn = getSecondaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/index.php'],
		serverPath = '/phoenixrobotix/src/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload the API files to secondary server
gulp.task('upload_api_files_to_secondary_server', () => {
	var conn = getSecondaryServerFtpConnection(),
		localFiles = [__dirname + '/dist/api/*.php'],
		serverPath = '/phoenixrobotix/api/datoms/iocl';
	return gulp.src(localFiles, {buffer: false})
		.pipe(conn.newer(serverPath))
		.pipe(conn.dest(serverPath));
});
// Upload files to primary server
gulp.task('upload_files_to_secondary_server', [
	/*'upload_js_files_to_secondary_server',
	'upload_php_files_to_secondary_server',
	'upload_api_files_to_secondary_server'*/
]);

// Upload the files to testing server
gulp.task('upload_files_to_testing_server', () => {
	// var conn = getTestingServerFtpConnection(),
	// 	localFiles = [
	// 		'./dist/**/*.*',
	// 		'!./dist/*.html',
	// 		'!./dist/*.js',
	// 		'!./dist/cdn/*.json'
	// 	],
	// 	serverPath = '/phoenixrobotix/dev/datoms/iocl';
	// return gulp.src(localFiles, {buffer: false})
	// 	.pipe(conn.newer(serverPath))
	// 	.pipe(conn.dest(serverPath));
});